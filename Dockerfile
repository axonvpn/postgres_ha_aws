FROM ubuntu:16.04
USER root
RUN \
    apt-get update -y && \
    apt-get install -y software-properties-common && \
    apt-add-repository ppa:ansible/ansible && \
    apt-get update -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    acl \
    ansible \
    nano \
    asciidoc \
    bzip2 \
    cdbs \
    curl \
    debhelper \
    debianutils \
    devscripts \
    docbook-xml \
    dpkg-dev \
    fakeroot \
    gawk \
    gcc \
    git \
    libffi-dev \
    libssl-dev \
    libxml2-utils \
    locales \
    make \
    mercurial \
    mysql-server \
    openssh-client \
    openssh-server \
    python-dev \
    pass \
    python-httplib2 \
    python-jinja2 \
    python-keyczar \
    python-lxml \
    python-mock \
    python-mysqldb \
    python-nose \
    python-paramiko \
    python-passlib \
    python-pip \
    python-setuptools \
    python-virtualenv \
    python-yaml \
    reprepro \
    rsync \
    ruby \
    sshpass \
    subversion \
    sudo \
    tzdata \
    unzip \
    xsltproc \
    zip \
    telnet \
    dnsmasq \
    && \
    apt-get clean

RUN pip install pip --upgrade
RUN pip install --upgrade pycrypto cryptography
RUN pip install sshpubkeys cs boto boto3
RUN locale-gen en_US.UTF-8
RUN pip install coverage junit-xml sshpubkeys cs
RUN mkdir /root/postgres-ha -p
# dnsmasq configuration
RUN echo 'listen-address=127.0.0.1' >> /etc/dnsmasq.conf
RUN echo 'resolv-file=/etc/resolv.dnsmasq.conf' >> /etc/dnsmasq.conf
RUN echo 'conf-dir=/etc/dnsmasq.d' >> /etc/dnsmasq.conf
RUN echo 'user=root' >> /etc/dnsmasq.conf
RUN echo 'nameserver 127.0.0.1' >> /etc/resolv.conf
RUN touch /etc/dnsmasq.d/0hosts
CMD ["/sbin/init"]
