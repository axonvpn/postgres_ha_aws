#Create instances in AWS
An Ansible playbook to deploy create instance in AWS
##Setup


### If run playbook in native instance
Make sure installed following software
```
ansible version >=2.5: apt-get install ansible -y
python-pip: apt-get install python-pip
pip install sshpubkeys cs boto boto3
```
### If run playbook in container (Recommend)
Build & run ansible image

```
sudo docker build . -t ansible
docker run -it -v /root/postgres_ha_aws:/root/postgres-ha --name ansible ansible /bin/bash
cd /root/postgres-ha
```
###API Key setup
You need to setup your API keys. The Key & secret can be found within your portal under the account section.

nano ~/.boto
```
[Credentials]
aws_access_key_id = 
aws_secret_access_key = 

```

###Variable setup
A few variables must be setup prior executing this playbook. These variables are located in the following file:

defaults/main.yml


##Running the playbook
To run the playbook:

`ansible-playbook create-infrastructre.yml`